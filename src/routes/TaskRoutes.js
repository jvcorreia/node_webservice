const express = require('express');
const router = express.Router();

const TaskController = require('../controller/TaskController');
const taskValidation = require('../middlewares/TaskValidator')

router.post('/new',taskValidation, TaskController.create);
router.put('/update/:id',taskValidation, TaskController.update);

module.exports = router;