const taskModel = require('../model/TaskModel')
const { isPast } = require('date-fns')


const taskValidation = async (req, res, next) => {
    const { macaddress, type, title, description, when } = req.body

    if (!macaddress || !type || !title || !description || !when) {
        return res.status(400).json('error: Existem campos não inseridos')
    }

    if (isPast(new Date(when))) {
        return res.status(400).json('error: Data não válida')
    }

    if (req.params.id) { //Verificando se existe um id nos paremtros
        let exists = await taskModel.findOne(
            {
                '_id': { '$ne': req.params.id },
                'when': { '$eq': new Date(when) },
                'macaddress': { '$eq': macaddress }
            })
    } else {

        let exists = await taskModel.findOne({
            'when': { '$eq': new Date(when) },
            'macaddress': { '$eq': macaddress }
        })
    }



    if (exists) {
        return res.status(400).json('error: Duas tarefas no mesmo horário')
    }



    next()
}

module.exports = taskValidation