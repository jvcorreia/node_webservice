const express = require('express'); //Requerendo a lib
const app = express(); //Instanciando a lib
app.use(express.json());


const TaskRoutes = require('./routes/TaskRoutes');
app.use('/task', TaskRoutes);


app.listen(3000, () => console.log('Server rodando na porta 3000'));


